import pandas as pd
import matplotlib.pyplot as plt

fn = "small-rc17.txt"
fn = "small-rc16.txt"
d = pd.read_csv( fn, sep=";", header=None)

d.columns = ['id', "sch_type", "sch_name", "dist_name", "city", "county", "dist_lowinc"]

fn = "small-rc17-ass-math.txt" # change me to do ela, and change the output file name at the end.
fn = "small-rc16-ass-math.txt" 
fn = "small-rc16-ass-ela.txt" 
da = pd.read_csv( fn, sep=";", header=None)
da.columns = ['id', "sch_type", "sch_name", "dist_name", "grades", "enrollment",
              "dist_not_meet", "dist_partial", "dist_approach", "dist_met",
              "dist_exceed"]
keep_me = ['id', "dist_not_meet", "dist_partial", "dist_approach", "dist_met",
           "dist_exceed"]
da = da[keep_me]

m = pd.merge( d, da)

# only cook county

m = m[m.county == "Cook       "]
m = m[m.city != 'Chicago          ']

m = m[[ u'dist_name', u'county',
       u'dist_lowinc', u'dist_not_meet', u'dist_partial', u'dist_approach',
       u'dist_met', u'dist_exceed']]

m = m.drop_duplicates()
for cc in [u'dist_not_meet', u'dist_partial', u'dist_approach',
           u'dist_met', u'dist_exceed']:
    m[cc] = pd.to_numeric(m[cc], errors='coerce')

m = m.dropna()

m['meet_exceed'] = m.dist_met + m.dist_exceed
plt.plot( m.dist_lowinc, m.meet_exceed, 'o')

poly = pd.np.polyfit( m.dist_lowinc, m.meet_exceed, deg=1)

line_vals = pd.np.polyval(poly, m.dist_lowinc)
plt.plot( m.dist_lowinc, line_vals)

# residuals
m['reg_model'] = line_vals
m['resid'] = m.meet_exceed - line_vals

m = m.sort_values(by="resid")

#m.to_csv('sub_cook_math_2016.csv', sep="\t") # change the file name to do ela
m.to_csv('sub_cook_ela_2016.csv', sep="\t") # change the file name to do ela
#run assessgr8.py # the command to do in ipython interpreter.

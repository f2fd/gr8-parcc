## strip out the fields we need from the massive isbe datafiles
## those are at https://www.isbe.net/Pages/Illinois-State-Report-Card-Data.aspx
## and are rc17_assessment.zip and rc17.zip
## we unzip them to rc17_assessment.txt and rc17.txt and then run awk to pull out
## the fields we want. That website has a excel file that shows what columns
## have which fields. Hence columns 3805-3809 have the PARCC math % of students
## at varios levels.
## uncomment the commands starting with cat that you want to run
## then on a mac, on the command line do
## prompt> . ./grepme.sh

## small-rc17-ass.txt is the assessment data for eal
## small-rc17-ass-man.txt is the assessment data for math
## small-rc17.txt is the file we need that has the county and % low income per district

#cat rc17_assessment.txt |awk -F";" '{print $1";"$2";"$4";"$5";"$6";"$7";"$3785";"$3786";"$3787";"$3788";"$3789}' > small-rc17-ass.txt

#cat rc17_assessment.txt |awk -F";" '{print $1";"$2";"$4";"$5";"$6";"$7";"$3805";"$3806";"$3807";"$3808";"$3809}' > small-rc17-ass-math.txt

#cat rc17.txt |awk -F";" '{print $1";"$2";"$4";"$5";"$6";"$7";"$56}' > small-rc17.txt

#2016 -- ela and math fields
e0=3713;e1=3714;e2=3715;e3=3716;e4=3717;
m0=3733;m1=3734;m2=3735;m3=3736;m4=3737

#cat rc16_assessment.txt |awk -F";" -v e0="$e0" -v e1="$e1" -v e2="$e2" -v e3="$e3" -v e4="$e4" '{print $1";"$2";"$4";"$5";"$6";"$7";"$e0";"$e1";"$e2";"$e3";"$e4}' > small-rc16-ass.txt

#cat rc16_assessment.txt |awk -F";" -v e0="$m0" -v e1="$m1" -v e2="$m2" -v e3="$m3" -v e4="$m4" '{print $1";"$2";"$4";"$5";"$6";"$7";"$e0";"$e1";"$e2";"$e3";"$e4}' > small-rc16-ass-math.txt


cat rc16.txt |awk -F";" '{print $1";"$2";"$4";"$5";"$6";"$7";"$56}' > small-rc16.txt


# README #

### What is this repository for? ###

This repository has scripts to extract fields from files from the
Illinois State Board of Education website associated with the Illinois
Report Card. Also there are some python files that illustrate
analysis.

The specific question addressed is to look at district level Grade 8
PARCC scores in Suburban Cook County in a particular year (eg. 2017 or
2016), and control for income level of students in the district. In
other words regress PARCC score against income level and look at the
residuals.

### How do I get set up? ###

The scripts assume you have the files from the data page of the ISBE
Report Card
[website](https://www.isbe.net/Pages/Illinois-State-Report-Card-Data.aspx).

You need the Report [Card
Data](https://www.isbe.net/Documents/rc17.zip), and the [Report Card
Data with Assessment
Data](https://www.isbe.net/Documents/rc17_assessment.zip).

Those files are zip files. Unzip them and they are ready to be read by
the scripts in this repository.

### Other analyses

If you want to run other analyses with other fields you will need to
modify the scripts to pull out the new fields you want. The scripts
just pull out the field by column number. For example the Grade 8 math
PARCC scores are in columns 3805-3809.  Modify the script to pull out
the fields you want.

To find the fields and their column number, ISBE provides an [excel
file](https://www.isbe.net/_layouts/Download.aspx?SourceUrl=/Documents/RC17_layout_10-30-17.xlsx)
that tells you the layout.

And then obviously you would need to modify the python analysis
programs with the specifics for your analysis. But having the data
fields you want, you can use your favorite analysis tool.

### Configuration

The scripts use unix commands cat and awk to extract the fields from
the data files.  The analysis is done with python.

### Dependencies

The python scripts import pandas and matplotlib.
